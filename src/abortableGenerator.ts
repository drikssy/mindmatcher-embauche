import {parser} from 'stream-json'
import {streamArray} from "stream-json/streamers/StreamArray";
import * as fs from "fs";
import * as path from "node:path";
export default async function* abortableGenerator(shouldAbortFn: (count: number) => boolean): AsyncGenerator<any, void, unknown> {

  const files = (fs.readdirSync('data/current')).map((fileName: string) => path.join('data/current', fileName));
  let count = 0;
  for (const file of files) {
    const jsonStream = (fs.createReadStream(file)
        .pipe(parser())
        .pipe(streamArray()))
    for await (const {value} of jsonStream) {
      if (shouldAbortFn(count)) {
        jsonStream.destroy();
        console.log('Generator loop was gracefully aborted')
        return;
      }
      count++;
      yield value;
    }
  }
}
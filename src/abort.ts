
let abort = false
export function shouldAbortTime(timeoutms = 1000): () => boolean {
  
  setTimeout(() => abort = true, timeoutms)

  return () => {
    
    return abort
  }

}

export function shouldAbortCountMax(countMax: number = 500): (count: number) => boolean {
  return (count: number) => count > countMax
}
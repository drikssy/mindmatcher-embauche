import {shouldAbortCountMax} from "./abort";
import abortableGenerator from "./abortableGenerator";

async function main() {
    for await (const document of abortableGenerator(abortFn)) {

        console.log('Processing document', document.id)
        await new Promise(resolve => setTimeout(resolve, 1));

    }
}

console.log('===== Start processing')

const countMax = 6000 // do not hesitate to increase it for more chunks & documents processing
const abortFn = shouldAbortCountMax(countMax)

main()
    .catch(console.log)
    .finally(() => {
        console.log('===== script is ended!')
        process.exit(0)
    })

